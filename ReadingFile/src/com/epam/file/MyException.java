package com.epam.file;

public class MyException extends Exception {

    public MyException(Throwable cause) {
        super(cause);
    }
}
